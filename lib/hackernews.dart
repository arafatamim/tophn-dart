library hackernews;

import 'dart:convert';
import 'dart:isolate';

import 'package:http/http.dart' as http;

class Restaurant {
  const Restaurant({required this.name, required this.cuisine});
  final String name;
  final String cuisine;
}

class Story {
  final String title;
  final String url;
  final int score;
  final String by;

  const Story({
    required this.title,
    required this.url,
    required this.score,
    required this.by,
  });

  factory Story.fromJson(Map<String, dynamic> data) {
    final title = data['title'] as String;
    final url = data['url'] as String;
    final score = data["score"] as int;
    final by = data["by"] as String;

    return Story(
      title: title,
      url: url,
      score: score,
      by: by,
    );
  }
}

const storiesUrl = "https://hacker-news.firebaseio.com/v0/beststories.json";
const itemUrlBase = "https://hacker-news.firebaseio.com/v0/item";

void main() async {
  final res = await http.get(Uri.parse(storiesUrl));
  final ids = (jsonDecode(res.body) as List).map((v) => v as int).toList();
  final topFive = ids.sublist(0, 5);

  final p = ReceivePort();
  for (final id in topFive) {
    await Isolate.spawn(
      getStory,
      [p.sendPort, id],
    );
  }
  p.take(5).listen((message) {
    final story = message as Story;
    print(
      "${story.title}\nScore: ${story.score}\nBy: ${story.by}\nURL: ${story.url}\n",
    );
  });
}

getStory(List args) {
  final SendPort p = args[0];
  final int id = args[1];
  http.get(Uri.parse("$itemUrlBase/$id.json")).then((res) {
    final story = Story.fromJson(jsonDecode(res.body));
    return story;
  }).then(
    (value) => Isolate.exit(p, value),
  );
}
